/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SampleTest;

/**
 *
 * @author Zaakir
 */



import io.appium.java_client.android.AndroidDriver;
import java.io.File;
import java.net.URL;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Appium 
{
     
    public static AndroidDriver driver;
    
    public static Runtime rt = Runtime.getRuntime();
    
    public static void runAppiumTest()
    {
         driver = (AndroidDriver) runAppiumTest(System.getProperty("user.dir")+"\\appiumTest\\Application\\Byte Orbit app-debug.apk", "emulator-5554");
    }
    
    public static WebDriver runAppiumTest(String appPath, String deviceUDID)
    {
        try
        {
            rt.exec("cmd.exe /c cd & start cmd.exe \"appium\" /k \" appium -a 127.0.0.1 -p 4723 -U emulator-5554 \"");

            //The APK information is set in the appconfig
            File app = new File(appPath);
            
            DesiredCapabilities capabilities = new DesiredCapabilities();

            capabilities.setCapability("udid", "emulator-5554");
            capabilities.setCapability("deviceName", "emulator-5554");
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("automationName", "Appium");
//            capabilities.setCapability(CapabilityType.VERSION, "6.0");

            //Call the following use RunActivity
            capabilities.setCapability("appPackage", "com.byteorbit.qaassignment1");
            capabilities.setCapability("appActivity", "com.byteorbit.qaassignment1.LoginActivity");
            capabilities.setCapability("clearSystemFiles", "true");
            
            capabilities.setCapability("newCommandTimeout", 360);

            return new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
        }

        catch (Exception e)
        {
            System.out.println("Error setting Capabilities - " + e.getMessage());
            return null;
        }
    }
     
}
